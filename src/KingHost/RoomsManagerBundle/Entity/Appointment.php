<?php

namespace KingHost\RoomsManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use KingHost\UserAdminBundle\Entity\Guy;

/**
 * Appointment
 *
 * @ORM\Table(
 *     name="appointment",
 *     indexes={
 *     @ORM\Index(name="idx_fe38f8441a3602ca", columns={"guy_id"}),
 *     @ORM\Index(name="idx_fe38f84454177093", columns={"room_id"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="KingHost\RoomsManagerBundle\Repository\AppointmentRepository")
 */
class Appointment
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_appointment", type="datetime")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $dtAppointment;

    /**
     * @var \KingHost\RoomsManagerBundle\Entity\Room
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="KingHost\RoomsManagerBundle\Entity\Room")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="room_id", referencedColumnName="room_id")
     * })
     */
    private $room;

    /**
     * @var \KingHost\UserAdminBundle\Entity\Guy
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="KingHost\UserAdminBundle\Entity\Guy")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="guy_id", referencedColumnName="guy_id")
     * })
     */
    private $guy;



    /**
     * Set dtAppointment
     *
     * @param \DateTime $dtAppointment
     *
     * @return Appointment
     */
    public function setDtAppointment($dtAppointment)
    {
        $this->dtAppointment = $dtAppointment;

        return $this;
    }

    /**
     * Get dtAppointment
     *
     * @return \DateTime
     */
    public function getDtAppointment()
    {
        return $this->dtAppointment;
    }

    /**
     * Set room
     *
     * @param Room $room
     *
     * @return Appointment
     */
    public function setRoom(Room $room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return Room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set guy
     *
     * @param Guy $guy
     *
     * @return Appointment
     */
    public function setGuy(Guy $guy)
    {
        $this->guy = $guy;

        return $this;
    }

    /**
     * Get guy
     *
     * @return Guy
     */
    public function getGuy()
    {
        return $this->guy;
    }
}