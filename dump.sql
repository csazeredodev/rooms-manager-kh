--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: appointment; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE appointment (
    dt_appointment timestamp(0) without time zone NOT NULL,
    room_id integer NOT NULL,
    guy_id integer NOT NULL
);


ALTER TABLE public.appointment OWNER TO postgres;

--
-- Name: guy; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE guy (
    guy_id integer NOT NULL,
    username character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    salt character varying(255) NOT NULL,
    roles text NOT NULL,
    isactive boolean NOT NULL,
    email character varying(255) NOT NULL
);


ALTER TABLE public.guy OWNER TO postgres;

--
-- Name: COLUMN guy.roles; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN guy.roles IS '(DC2Type:array)';


--
-- Name: guy_guy_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE guy_guy_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.guy_guy_id_seq OWNER TO postgres;

--
-- Name: room; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE room (
    room_id integer NOT NULL,
    name character varying(255) NOT NULL,
    capacity integer NOT NULL
);


ALTER TABLE public.room OWNER TO postgres;

--
-- Name: room_room_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE room_room_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.room_room_id_seq OWNER TO postgres;

--
-- Data for Name: appointment; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY appointment (dt_appointment, room_id, guy_id) FROM stdin;
\.


--
-- Data for Name: guy; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY guy (guy_id, username, password, salt, roles, isactive, email) FROM stdin;
1	User	RRBwXFp7fnGbUX5yWi0DVLqzPpc8dF/CXoqD+FK0UFBdKp86nsA+rl9VZppW4cquRR0q0OWUeAX+Tv4ydvFjcg==	aba5e23e9eedf800000000000000000000000000	a:0:{}	t	user@user
2	admin	Iy+StOAW//0kXbcVSBzoBGqFByq/kKPpK6+zjbWufwZMKEpJA6Is9AOCUSGeaNXn3eEU+3gxfW2ERP1Ykf16lw==	e6e18ffa45eb2800000000000000000000000000	a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}	t	admin@admin
\.


--
-- Name: guy_guy_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('guy_guy_id_seq', 2, true);


--
-- Data for Name: room; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY room (room_id, name, capacity) FROM stdin;
1	Starts	200
2	Targaryen	500
\.


--
-- Name: room_room_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('room_room_id_seq', 2, true);


--
-- Name: appointment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY appointment
    ADD CONSTRAINT appointment_pkey PRIMARY KEY (dt_appointment, room_id, guy_id);


--
-- Name: guy_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY guy
    ADD CONSTRAINT guy_pkey PRIMARY KEY (guy_id);


--
-- Name: room_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY room
    ADD CONSTRAINT room_pkey PRIMARY KEY (room_id);


--
-- Name: idx_fe38f8441a3602ca; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_fe38f8441a3602ca ON appointment USING btree (guy_id);


--
-- Name: idx_fe38f84454177093; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX idx_fe38f84454177093 ON appointment USING btree (room_id);


--
-- Name: uniq_729f519b5e237e06; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX uniq_729f519b5e237e06 ON room USING btree (name);


--
-- Name: uniq_c948409ce7927c74; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX uniq_c948409ce7927c74 ON guy USING btree (email);


--
-- Name: uniq_c948409cf85e0677; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE UNIQUE INDEX uniq_c948409cf85e0677 ON guy USING btree (username);


--
-- Name: fk_fe38f8441a3602ca; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY appointment
    ADD CONSTRAINT fk_fe38f8441a3602ca FOREIGN KEY (guy_id) REFERENCES guy(guy_id);


--
-- Name: fk_fe38f84454177093; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY appointment
    ADD CONSTRAINT fk_fe38f84454177093 FOREIGN KEY (room_id) REFERENCES room(room_id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

