<?php

namespace KingHost\RoomsManagerBundle\Datafixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KingHost\RoomsManagerBundle\Entity\Room;
use KingHost\UserAdminBundle\Entity\Guy;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadUser
 * @package KingHost\UserAdminBundle\Datafixtures\ORM
 */
class LoadRoom implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $firstRoom = new Room();
        $firstRoom->setName("Starts");
        $firstRoom->setCapacity(200);
        $manager->persist($firstRoom);

        $secondRoom = new Room();
        $secondRoom->setName("Targaryen");
        $secondRoom->setCapacity(500);
        $manager->persist($secondRoom);

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }


}