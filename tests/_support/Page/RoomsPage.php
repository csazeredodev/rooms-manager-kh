<?php
namespace Page;

class RoomsPage
{
    // include url of current page
    public static $URL = '/app';

    /**
     * @param $param
     * @return string
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }


}
