<?php
/**
 * Created by PhpStorm.
 * User: cleber
 * Date: 28/04/16
 * Time: 10:24
 */

namespace KingHost\RoomsManagerBundle\Model;

use Doctrine\Common\Persistence\ObjectManager;
use KingHost\RoomsManagerBundle\Entity\Appointment;
use KingHost\RoomsManagerBundle\Entity\Room;
use KingHost\RoomsManagerBundle\Repository\AppointmentRepository;
use KingHost\UserAdminBundle\Entity\Guy;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class AppointmentBook
 * @package KingHost\RoomsManagerBundle\Model
 */
class AppointmentBook
{
    private $repository;
    private $em;


    /**
     * AppointmentBook constructor.
     * @param AppointmentRepository $repository
     * @param ObjectManager $em
     */
    public function __construct(AppointmentRepository $repository, ObjectManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @param array $unavailableTime
     * @return array
     */
    public static function generateCommercialTime(array $unavailableTime = array())
    {
        $time = [
            '08:00:00',
            '09:00:00',
            '10:00:00',
            '11:00:00',
            '12:00:00',
            '13:00:00',
            '14:00:00',
            '15:00:00',
            '16:00:00',
            '17:00:00',
            '18:00:00',
        ];

        dump(array_diff($time, array()));
    }
}