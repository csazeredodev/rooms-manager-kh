<?php

namespace KingHost\RoomsManagerBundle\Datafixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KingHost\RoomsManagerBundle\Entity\Appointment;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class LoadUser
 * @package KingHost\UserAdminBundle\Datafixtures\ORM
 */
class LoadAppointment implements FixtureInterface, OrderedFixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

    }

    private function getGuy(ObjectManager $em)
    {
        return $em->getRepository('UserAdminBundle:Guy')
                ->findOneBy(array('id' => 1));

    }

    private function getRoom(ObjectManager $em)
    {
        return $em->getRepository('RoomsManagerBundle:Room')
            ->findOneBy(array('id' => 1));
    }

    public function getOrder()
    {
        return 3;
    }
}