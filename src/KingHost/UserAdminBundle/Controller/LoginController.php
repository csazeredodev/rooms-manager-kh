<?php

namespace KingHost\UserAdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class LoginController extends Controller
{
    /**
     * @Route("/login")
     * @Template()
     * @param Request $request
     * @return array
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();

        $error = $session->get(Security::AUTHENTICATION_ERROR);
        $session->remove(Security::AUTHENTICATION_ERROR);

        return array(
            'lastUsername' => $session->get(Security::LAST_USERNAME),
            'error' => $error
        );
    }

    /**
     * @Route("/login_check")
     */
    public function loginCheckAction()
    {

    }

    /**
     * @Route("/logout")
     */
    public function logoutAction()
    {

    }
}
