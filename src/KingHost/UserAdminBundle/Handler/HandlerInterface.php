<?php
namespace KingHost\UserAdminBundle\Handler;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class HandlerInterface
 * @package KingHost\UserAdminBundle\Handler
 */
interface HandlerInterface
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request);

    /**
     * @param Request $request
     * @param $entity
     * @return mixed
     */
    public function update(Request $request, $entity);

    /**
     * @return array
     */
    public function all();
}
