<?php

namespace KingHost\RoomsManagerBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use KingHost\RoomsManagerBundle\Entity\Room;

/**
 * Room controller.
 *
 * @Route("/room")
 */
class RoomController extends Controller
{
    /**
     * Lists all Room entities.
     *
     * @Route("/")
     * @Template()
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $rooms = $em->getRepository('RoomsManagerBundle:Room')->findAll();

        return array(
            'rooms' => $rooms,
        );
    }

    /**
     * Creates a new Room entity.
     *
     * @Route("/new")
     * @Template()
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $hasSubmit = $request->request->has('submit');

        if ($hasSubmit) {
            $handler = $this->getHandler();

            try {
                $room = $handler->create($request);
                return $this->redirectToRoute('kinghost_roomsmanager_room_show', array('id' => $room->getId()));

            } catch (\InvalidArgumentException $e) {
                return array(
                    'error' => $e->getMessage()
                );
            }
        }

        return array(
            'error' => ''
        );

    }

    /**
     * Finds and displays a Room entity.
     *
     * @Route("/{id}")
     * @Template()
     * @Method("GET")
     */
    public function showAction(Room $room)
    {
        $deleteForm = $this->createDeleteForm($room);

        return array(
            'room' => $room,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Room entity.
     *
     * @Route("/{id}/edit")
     * @Template()
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Room $room)
    {
        $hasSubmit = $request->request->has('submit');

        if ($hasSubmit) {
            $handler = $this->getHandler();

            try {
                $room = $handler->update($request, $room);
                return $this->redirectToRoute('kinghost_roomsmanager_room_show', array('id' => $room->getId()));

            } catch (\InvalidArgumentException $e) {
                return array(
                    'error' => $e->getMessage(),
                    'room' => $room
                );
            }
        }

        return array(
            'error' => '',
            'room' => $room
        );
    }

    /**
     * Deletes a Room entity.
     *
     * @Route("/{id}")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Room $room)
    {
        $form = $this->createDeleteForm($room);
        $form->handleRequest($request);

        dump($form->isValid());

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($room);
            $em->flush();
        }

        return $this->redirectToRoute('kinghost_roomsmanager_room_index');
    }

    /**
     * Creates a form to delete a Room entity.
     *
     * @param Room $room The Room entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Room $room)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('kinghost_roomsmanager_room_delete', array('id' => $room->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    private function getHandler()
    {
        return $this->get('khrm.manager.room_handler');
    }
}
