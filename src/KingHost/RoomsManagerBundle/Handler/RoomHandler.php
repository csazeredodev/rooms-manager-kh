<?php

namespace KingHost\RoomsManagerBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use KingHost\RoomsManagerBundle\Entity\Room;
use KingHost\RoomsManagerBundle\Repository\RoomRepository;
use KingHost\UserAdminBundle\Handler\HandlerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class RoomHandler
 * @package KingHost\RoomsManagerBundle\Handler
 */
class RoomHandler implements HandlerInterface
{
    private $repository;
    private $em;

    /**
     * RoomHandler constructor.
     * @param RoomRepository $repository
     * @param ObjectManager $em
     */
    public function __construct(RoomRepository $repository, ObjectManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @return Room
     */
    public function create(Request $request)
    {
        $parameters = $request->request;

        $room = new Room();
        $room->setName($parameters->get('roomname'));
        $room->setCapacity($parameters->get('capacity'));

        try {
            $this->em->persist($room);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new \InvalidArgumentException('There is already a room with that name');
        }

        return $room;
    }

    /**
     * @param Request $request
     * @param Room $room
     * @return Room
     */
    public function update(Request $request, $room)
    {
        $parameters = $request->request;

        $room->setName($parameters->get('roomname'));
        $room->setCapacity($parameters->get('capacity'));

        try {
            $this->em->merge($room);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new \InvalidArgumentException('Username or email already exists in database');
        }

        return $room;
    }

    public function all()
    {
        // TODO: Implement all() method.
    }

}