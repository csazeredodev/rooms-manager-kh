<?php

namespace KingHost\RoomsManagerBundle\Controller;

use KingHost\RoomsManagerBundle\Model\AppointmentBook;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AppointmentController
 * @Route("/appointment")
 * @package KingHost\RoomsManagerBundle\Controller
 */
class AppointmentController extends Controller
{
    /**
     * @Method("GET")
     * @Template()
     * @Route("/")
     */
    public function indexAction()
    {
        $repository = $this->getRepository();
        $data = $repository->findAllAppointmentsFormatted();

        return array(
            'data' => $data
        );
    }

    /**
     * @Route("/new")
     * @Template()
     * @Method({"GET", "POST"})
     * @param Request $request
     * @return array
     */
    public function newAction(Request $request)
    {
        $repository = $this->getRepository();
        $unavailableTime = $repository->findAppointmentByDate(new \DateTime());

        dump($unavailableTime);

        return array(
            '' => ''
        );
    }

    private function getRepository()
    {
        $em = $this->getDoctrine()->getEntityManager();
        return $em->getRepository('RoomsManagerBundle:Appointment');
    }
}

