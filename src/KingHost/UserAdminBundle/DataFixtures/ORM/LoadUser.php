<?php

namespace KingHost\UserAdminBundle\Datafixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use KingHost\UserAdminBundle\Entity\Guy;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LoadUser
 * @package KingHost\UserAdminBundle\Datafixtures\ORM
 */
class LoadUser implements FixtureInterface, ContainerAwareInterface, OrderedFixtureInterface
{
    private $container;
    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $user = new Guy();
        $user->setUsername("User");
        $user->setPassword($this->encodePassword($user, "user"));
        $user->setIsActive(true);
        $user->setEmail("user@user");
        $manager->persist($user);

        $admin = new Guy();
        $admin->setUsername("admin");
        $admin->setPassword($this->encodePassword($admin, "admin"));
        $admin->setIsActive(true);
        $admin->setEmail("admin@admin");
        $admin->setRoles(array('ROLE_SUPER_ADMIN'));
        $manager->persist($admin);

        $manager->flush();
    }

    /**
     * @param Guy $guy
     * @param $plaintext
     * @return mixed
     */
    public function encodePassword(Guy $guy, $plaintext)
    {
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($guy);
        return $encoder->encodePassword($plaintext, $guy->getSalt());
    }

    public function getOrder()
    {
        return 2;
    }
}