<?php

namespace KingHost\UserAdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use KingHost\UserAdminBundle\Entity\Guy;
use KingHost\UserAdminBundle\Form\GuyType;

/**
 * Guy controller.
 *
 * @Route("/admin/user")
 */
class GuyController extends Controller
{
    /**
     * Lists all Guy entities.
     *
     * @Route("/")
     * @Template()
     * @Method("GET")
     */
    public function indexAction()
    {
        $handler = $this->getHandler();
        $guys = $handler->all();

        return array(
            'guys' => $guys,
        );
    }

    /**
     * Creates a new Guy entity.
     *
     * @Route("/new")
     * @Template()
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $hasSubmit = $request->request->has('submit');
        $guy = new Guy();

        if ($hasSubmit) {
            $handler = $this->getHandler();

            try {
                $guy = $handler->create($request);
                return $this->redirectToRoute('kinghost_useradmin_guy_show', array('id' => $guy->getId()));

            } catch (\InvalidArgumentException $e) {
                return array(
                    'error' => $e->getMessage()
                );
            }
        }

        return array(
            'error' => ''
        );
    }

    /**
     * Finds and displays a Guy entity.
     *
     * @Route("/{id}")
     * @Template()
     * @Method("GET")
     */
    public function showAction(Guy $guy)
    {
        $deleteForm = $this->createDeleteForm($guy);

        return array(
            'guy' => $guy,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Guy entity.
     *
     * @Route("/{id}/edit")
     * @Template()
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Guy $guy)
    {
        $hasSubmit = $request->request->has('submit');

        if ($hasSubmit) {
            $handler = $this->getHandler();

            try {
                $guy = $handler->update($request, $guy);
                return $this->redirectToRoute('kinghost_useradmin_guy_show', array('id' => $guy->getId()));

            } catch (\InvalidArgumentException $e) {
                return array(
                    'error' => $e->getMessage(),
                    'guy' => $guy
                );
            }
        }

        return array(
            'error' => '',
            'guy' => $guy
        );
    }

    /**
     * Deletes a Guy entity.
     *
     * @Route("/{id}", name="user_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Guy $guy)
    {
        $form = $this->createDeleteForm($guy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($guy);
            $em->flush();
        }

        return $this->redirectToRoute('kinghost_useradmin_guy_index');
    }

    /**
     * Creates a form to delete a Guy entity.
     *
     * @param Guy $guy The Guy entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Guy $guy)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('user_delete', array('id' => $guy->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    /**
     * @return \KingHost\UserAdminBundle\Handler\GuyHandler
     */
    private function getHandler()
    {
        return $this->get('khrm.admin.user_handler');
    }
}
