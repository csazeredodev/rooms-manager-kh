<?php

$I = new AcceptanceTester($scenario);
$I->wantTo('Login using email');
$I->amOnPage('/login');
$I->fillField('#username', 'admin@admin');
$I->fillField('#passwd', 'admin');
$I->click('Login');
$I->canSeeCurrentUrlEquals('/app');