<?php

namespace KingHost\UserAdminBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use KingHost\UserAdminBundle\Entity\Guy;
use KingHost\UserAdminBundle\Repository\GuyRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

/**
 * Class GuyHandler
 * @package KingHost\UserAdminBundle\Handler
 */
class GuyHandler implements HandlerInterface
{
    private $repository;
    private $em;
    private $encoder;

    /**
     * GuyHandler constructor.
     * @param GuyRepository $repository
     * @param ObjectManager $em
     */
    public function __construct(GuyRepository $repository, ObjectManager $em, MessageDigestPasswordEncoder $encoder)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->encoder = $encoder;
    }

    public function create(Request $request)
    {
        $parameters = $request->request;

        $guy = new Guy();

        $password = $this->cipherPassword($parameters->get('password'), $guy->getSalt());

        $guy->setUsername($parameters->get('username'));
        $guy->setPassword($password);
        $guy->setEmail($parameters->get('email'));
        $guy->setIsActive($parameters->has('active'));
        $guy->setRoles(array($parameters->get('role')));

        try {
            $this->em->persist($guy);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new \InvalidArgumentException('Username or email already exists in database');
        }


        return $guy;
    }

    /**
     * @param Request $request
     * @param $guy
     * @return mixed
     */
    public function update(Request $request, $guy)
    {
        $parameters = $request->request;
        $password = $this->cipherPassword($parameters->get('password'), $guy->getSalt());

        $guy->setUsername($parameters->get('username'));
        $guy->setEmail($parameters->get('email'));
        $guy->setPassword($password);
        $guy->setIsActive($parameters->has('active'));
        $guy->setRoles(array($parameters->get('role')));

        try {
            $this->em->merge($guy);
            $this->em->flush();
        } catch (UniqueConstraintViolationException $e) {
            throw new \InvalidArgumentException('Username or email already exists in database');
        }

        return $guy;
    }

    public function delete(Request $request)
    {
        // TODO: Implement delete() method.
    }

    public function get(Request $request)
    {
        // TODO: Implement get() method.
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->repository->findAll();
    }

    private function cipherPassword($plaintext, $salt)
    {
        return $this->encoder->encodePassword($plaintext, $salt);
    }
}
