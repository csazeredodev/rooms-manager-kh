<?php

$I = new AcceptanceTester($scenario);
$I->wantTo('Login using username');
$I->amOnPage('/login');
$I->fillField('#username', 'user');
$I->fillField('#passwd', 'user');
$I->click('Login');
$I->canSeeCurrentUrlEquals('/app');