<?php

namespace KingHost\UserAdminBundle\Repository;

use \Doctrine\ORM\EntityRepository;
use KingHost\UserAdminBundle\Entity\Guy;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * GuyRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GuyRepository extends EntityRepository implements UserProviderInterface, UserLoaderInterface
{

    public function loadUserByUsername($username)
    {
        $guy = $this->findOneByUsernameOrEmail($username);

        if (!$guy) {
            throw new UsernameNotFoundException("There is no user registered with this username");
        }

        return $guy;
    }

    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);

        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException("The instance of class $class is not supported");
        }

        return $user;
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return ($this->getEntityName() == $class);
    }

    /**
     * @param $usernameOrEmail
     * @return Guy|Null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    private function findOneByUsernameOrEmail($usernameOrEmail)
    {
        $guy = $this->createQueryBuilder("g")
            ->where("g.username = :username OR g.email = :email")
            ->setParameter("username", $usernameOrEmail)
            ->setParameter("email", $usernameOrEmail)
            ->getQuery()
            ->getOneOrNullResult();

        return $guy;
    }
}
